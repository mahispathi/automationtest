package in.edu.sreenidhi;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeeLoginTest {
	public static void main(String args[]) throws Exception{
	System.setProperty("webdriver.ie.driver", "drivers\\IEDriverServer.exe");
	System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
	System.setProperty("webdriver.gecko.driver", "drivers\\geckodriver-V0.16.1\\geckodriver.exe");
	DesiredCapabilities cap = DesiredCapabilities.firefox();
	cap.setCapability("marionette", true);
	WebDriver driver=null;
	String sBrowser="remote";
	if(sBrowser=="firefox")
		driver=new FirefoxDriver(cap);
	else if(sBrowser=="chrome")
		driver=new ChromeDriver();
	else if(sBrowser=="ie")
		driver=new InternetExplorerDriver();
	else{
		DesiredCapabilities desiredCapabilities=DesiredCapabilities.firefox();;
		driver=new RemoteWebDriver(new URL("http://localhost:4545/wd/hub"), desiredCapabilities);
	}
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	driver.get("http://sreenidhi.edu.in/");
	driver.findElement(By.id("user")).sendKeys("TS1049");
	driver.findElement(By.id("password")).sendKeys("ts1049");
	WebElement ele=driver.findElement(By.id("type"));
	
	Select select =new Select(ele);
	List<WebElement> opt=select.getOptions();
	select.selectByValue("Teacher");
	driver.findElement(By.xpath("//input[@type='submit']")).click();
//	driver.findElement(null).s
//driver.quit();
	
	}

}
